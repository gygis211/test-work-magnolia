import { Advantages } from "./components/Advantages/Advantages.jsx";
import { Header } from "./components/Header/Header.jsx";
import { Navigation } from "./components/Navigation/Navigation.jsx";
import { Slider } from "./components/Slider/Slider.jsx";
import { Slogan } from "./components/Slogan/Slogan.jsx";

function App() {
  return (
    <>
      <Header />
      <Navigation />
      <Slider />
      <Slogan />
      <Advantages />
    </>
  );
}

export default App;
