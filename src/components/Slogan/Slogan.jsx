import "./slogan.css"
import quotes from '../../source/“.svg'


export const Slogan = () => {

    return (
        <div className="slogan">
            <img src={quotes} alt='"' />
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas dui id ornare arcu odio ut. In nibh mauris cursus mattis molestie a iaculis at.</p>
        </div>
    );
};