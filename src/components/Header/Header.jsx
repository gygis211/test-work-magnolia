import React from "react";
import './Header.css'
import logo from '../../source/logo.svg'

export const Header = () => {

  return (
    <header>
        <img src={logo} alt="logo" />
    </header>
  );
}