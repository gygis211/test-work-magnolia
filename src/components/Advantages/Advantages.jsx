import { Content } from "./Content"
import './Advantages.css'
import villa from "../../source/villa.svg"
import bedRoom from "../../source/noun-bed-room-5502864 1.svg"
import sequrity from "../../source/security.svg"
import plant from "../../source/plant.svg"
import pool from "../../source/noun-pool-1209762 1.svg"
import car from "../../source/car.svg"

const initalState = [
    {
        img: villa,
        title: 'Lorem ipsum dolor',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.'
    },
    {
        img: bedRoom,
        title: 'Lorem ipsum dolor',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas dui id ornare arcu odio ut. In nibh mauris cursus mattis molestie a iaculis at.'
    },
    {
        img: sequrity,
        title: 'Lorem ipsum dolor',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas dui id ornare arcu odio ut. In nibh mauris cursus mattis molestie a iaculis at. Risus sed vulputate odio ut enim. '
    },
    {
        img: plant, 
        title: 'Lorem ipsum dolor',        
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas dui id ornare arcu odio ut. In nibh mauris cursus mattis molestie a iaculis at.'
    },
    {
        img: pool,
        title: 'Lorem ipsum dolor',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
        img: car,
        title: 'Lorem ipsum dolor',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    }
]

export const Advantages = () => {
    return (
        <div className="advantages">
            <h1>Lorem ipsum</h1>
            {initalState.map((el, i) => 
                <Content key={i} img={el.img} title={el.title} text={el.text} />
            )}
        </div>
    )
}