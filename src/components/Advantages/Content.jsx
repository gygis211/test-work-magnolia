import './Content.css'

export const Content = ({img , title, text}) => {
    return (
        <div className="content">
            <img src={img} alt="icon" />
            <h2>{title}</h2>
            <p>{text}</p>
        </div>
    )
}