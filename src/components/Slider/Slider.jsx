import { useState } from "react";
import img from "../../source/1-1о 1.jpg"
import arrow from "../../source/arrow.svg"
import "./slider.css"

const sliderImage = [
    {
        img: img,
    },
    {
        img: img,
    },
    {
        img: img,
    },
];

export const Slider = () => {
    const [currentSlide, setCurrentSlide] = useState(0);
    const [touchStartX, setTouchStartX] = useState(null);

    const handleSlideChangeLeft = () => {
        if (currentSlide > 0) {
            setCurrentSlide(currentSlide - 1);
        }
    };

    const handleSlideChangeRight = () => {
        if (currentSlide < sliderImage.length - 1) {
            setCurrentSlide(currentSlide + 1);
        }
    };

    const handleTouchStart = (e) => {
        setTouchStartX(e.touches[0].clientX);
    };

    const handleTouchMove = (e) => {
        if (touchStartX === null) {
            return;
        }

        const touchEndX = e.touches[0].clientX;
        const deltaX = touchEndX - touchStartX;

        if (deltaX > 0) {
            handleSlideChangeLeft();
        } else if (deltaX < 0) {
            handleSlideChangeRight();
        }

        setTouchStartX(null);
    };


    return (
        <div className="slider"  onTouchStart={handleTouchStart} onTouchMove={handleTouchMove} >
            <div className="left-right">
                <img src={arrow} alt="arrow" onClick={handleSlideChangeLeft} className={`arrow to-left ${(currentSlide > 0) ? '' : 'disable'}`} />
                <img src={arrow} alt="arrow" onClick={handleSlideChangeRight} className={`arrow to-right ${(currentSlide < sliderImage.length - 1) ? '' : 'disable'}`} />
            </div>
            <div className="slide"style={{ transform: `translateX(-${currentSlide * 100}vw)` }}>
                {sliderImage.map((el, i) => (
                    <img key={i} src={el.img} alt="slide" />
                ))}
            </div>
        </div>
    );
};