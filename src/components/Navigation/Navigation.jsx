import React, { useState } from "react";
import './Navigation.css'
import burger from '../../source/меню бургер.svg'
import insta from '../../source/instagram.svg'
import whats from '../../source/whatsapp.svg'
import mail from '../../source/mail.svg'
import line from '../../source/Line 86.svg'
import vector from '../../source/Vector.svg'

export const Navigation = () => {
    const [selectedLanguage, setSelectedLanguage] = useState("EN");
    return (
        <div className="navigation">
            <div className="menu">
                <img src={burger} alt="menu" />
            </div>
            <div className="social">
                <img src={insta} alt="insta" />
                <img src={whats} alt="whats" />
                <img src={mail} alt="mail" />
            </div>
            <div className="menu-desctop">
                <ul>
                    <li>Главная</li>
                    <li>О Magnolia <img src={vector} alt="&#94;" /></li>
                    <li>Карта</li>
                    <li>Галерея <img src={vector} alt="&#94;" /></li>
                    <li>Партнеры</li>
                    <li>Контакты</li>
                </ul>
                </div>
            <div className="lang">
                <span className={`${selectedLanguage == 'RU' ? 'active' : ''}`} onClick={() => setSelectedLanguage("RU")}>RU </span>
                <img src={line} alt="/" />
                <span className={`${selectedLanguage == 'EN' ? 'active' : ''}`} onClick={() => setSelectedLanguage("EN")}> EN</span>
            </div>
        </div>
    );
}